let ts = ./types.dhall
let cfg = ./config.dhall
let Styles = ts.Styles
in
[ { name = "now"
  , path = "/home/user/todo-now.dhall"
  , style = None Styles }
, { name = "longterm"
  , path = "/home/user/todo-longterm.dhall"
  , style = Some ( cfg.styles // { normal_item = {fg = Some 15, bg = Some 22, bold = False} } ) }
]
