let c =
  { styles =
    { flagpole = {fg = Some 233, bg = None Natural, bold = False} 
    , number = {fg = Some 154, bg = Some 233, bold = True} 
    , urgent_flag = { fg = Some 160, bg = Some 233, bold = True}
    , description_flag = { fg = Some 82, bg = None Natural, bold = True}
    , normal_item = {fg = Some 231, bg = Some 23, bold = False}
    , urgent_item = {fg = Some 231, bg = Some 23, bold = True}
    , deadline_approaching_item = {fg = Some 17, bg = Some 11, bold = True}
    , past_deadline_item = {fg = Some 51, bg = Some 124, bold = True}
    , confirmation = { fg = Some 232, bg = Some 202, bold = True}
    , question = { fg = Some 231, bg = Some 233, bold = False}
    , notification = { fg = Some 232, bg = Some 33, bold = True}
    , printitem_brief = {fg = Some 17, bg = Some 11, bold = True}
    }
  }
let x = c.styles 
let numbers_background_matches_flagpole = assert : x.flagpole.fg === x.number.bg 
let urgent_flags_background_matches_flagpole = assert : x.flagpole.fg === x.urgent_flag.bg
in c
