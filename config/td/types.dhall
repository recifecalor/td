{ Styles =
    { confirmation : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , deadline_approaching_item :
        { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , description_flag :
        { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , flagpole : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , normal_item : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , notification : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , number : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , past_deadline_item :
        { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , question : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , urgent_flag : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , urgent_item : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    , printitem_brief : { bg : Optional Natural, bold : Bool, fg : Optional Natural }
    }
, ListSchema = 
    List 
    { brief: Text
    , description: Optional Text
    , postponed: Optional Text
    , deadline: Optional Text
    , urgent: Integer
    , url: Optional Text
    }
}  
