extern crate gtk;
extern crate glib;
extern crate gdk;
extern crate gio;
extern crate dirs;
extern crate gdk_sys;
extern crate serde_yaml;
extern crate serde_dhall;
extern crate ansi_term;
extern crate textwrap;

use clap::{App,Arg};
use std::{io,error,fmt};
use std::io::Write;
use ansi_term::Colour::*;
use ansi_term::{Style,ANSIStrings};
use gtk::prelude::*;
use gio::prelude::*;
use glib::clone;
use std::rc::Rc;
use serde_dhall::StaticType;
use chrono::{NaiveDate, Local};
use serde::{Serialize, Deserialize};
use dirs::home_dir;

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Copy, StaticType)]
struct TDStyle {
    fg: Option<u64>,
    bg: Option<u64>,
    bold: bool
}
#[derive(Debug, PartialEq, Serialize, Deserialize, StaticType,Clone)]
struct TDStyles {
    flagpole: TDStyle,
    normal_item: TDStyle,
    urgent_item: TDStyle,
    deadline_approaching_item: TDStyle,
    past_deadline_item: TDStyle,
    number: TDStyle,
    urgent_flag: TDStyle,
    description_flag: TDStyle,
    confirmation: TDStyle,
    question: TDStyle,
    notification: TDStyle,
    printitem_brief: TDStyle
}
#[derive(Debug, PartialEq, Serialize, Deserialize, StaticType)]
struct Conf {
    styles: TDStyles
}
#[derive(Debug, PartialEq, Serialize, Deserialize, StaticType)]
struct ToDoList {
    name: String,
    path: String,
    style: Option<TDStyles>
}
#[derive(Debug, PartialEq, Serialize, Deserialize, StaticType, Default, Clone)]
struct ToDo {
    brief: String,
    deadline: Option<String>,
    description: Option<String>,
    url: Option<String>,
    directory: Option<String>,
    postponed: Option<String>,
    urgent: u64
}

#[derive(Debug, Serialize, Deserialize, Default)]
struct GUIData {
    filepath: String,
    todoitem: ToDo
}

#[derive(Debug, Clone)]
struct BadFileKey(String);
impl fmt::Display for BadFileKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "BadFileKey: {}", self.0)
    }
}
impl error::Error for BadFileKey {}


fn mk_dhall(list: &Vec<ToDo>) -> String {
    serde_dhall::serialize::<Vec<ToDo>>(&list).static_type_annotation().to_string().unwrap()
}
fn get_list(p: &std::path::Path) -> Result<Vec<ToDo>, serde_dhall::Error> {
    serde_dhall::from_file(p).parse()
}

fn get_list_style<'a>(lists: &'a Vec<ToDoList>, p: &str, conf: &Conf) -> Result<TDStyles, BadFileKey> {
    lists.iter().filter(|l| { l.name == String::from(p) }).next().map(|k| { (&k.style).clone().unwrap_or(conf.styles.clone()) }).ok_or(BadFileKey(p.to_owned()))
}

fn get_list_path<'a>(lists: &'a Vec<ToDoList>, p: &str) -> Result<&'a std::path::Path, BadFileKey> {
    lists.iter().filter(|l| { l.name == String::from(p) }).next().map(|k| { std::path::Path::new(&k.path) }).ok_or(BadFileKey(p.to_owned()))
}
fn get_list_path_string(lists: &Vec<ToDoList>, p: &str) -> Result<String, BadFileKey> {
    lists.iter().filter(|l| { l.name == String::from(p) }).next().map(|k| { k.path.clone() }).ok_or(BadFileKey(p.to_owned()))
}
fn style(tds: TDStyle) -> Style {
    let TDStyle { fg, bg, bold} = tds;
    let mut s = Style::new();
    if let Some(x) = fg { s = s.fg(Fixed(x as u8)); } else {};
    if let Some(x) = bg { s = s.on(Fixed(x as u8)); } else {};
    if bold { s = s.bold() ; }
    s
}
fn style_b2f(tds: TDStyle) -> Style {
    let TDStyle { fg: _, bg, bold: _} = tds;
    let s = Style::new();
    bg.map(|x| s.fg(Fixed(x as u8))).unwrap_or(s)
}


fn print_list(todos: &Vec<(usize, &ToDo)>, now: NaiveDate, styles: &TDStyles) {
    let mut i = 0;
    for todo in todos.iter().map(|(_,x)| x) {
        i = i + 1;
        let mut urg_flag = "".to_string();
        for _ in 0..todo.urgent { urg_flag += "✱" };
        let desc_flag = (if let Some(_) = todo.description.as_ref().or(todo.url.as_ref()).or(todo.directory.as_ref()) {"○○○"} else {""}).to_string();
        let norm = if todo.urgent > 0 { style(styles.urgent_item) } else { style(styles.normal_item) };
        let todo_brief = match (todo.urgent, days_to_deadline(now, &todo)) {
            (_,Some(n)) => match n {
                1..=2 =>  vec![
                    Style::new()
                        .on(Fixed(styles.flagpole.fg.unwrap() as u8))
                        .fg(Fixed(styles.deadline_approaching_item.bg.unwrap() as u8))
                        .paint("◢"),
                    style(styles.deadline_approaching_item).paint(format!(" {:^m$} ",todo.brief.clone(), m=2)),
                    Style::new().fg(Fixed(styles.deadline_approaching_item.bg.unwrap() as u8)).paint("◤") 
                ],
                _ if n < 1 => vec![
                    Style::new()
                        .on(Fixed(styles.flagpole.fg.unwrap() as u8))
                        .fg(Fixed(styles.past_deadline_item.bg.unwrap() as u8))
                        .paint("◢"),
                    style(styles.past_deadline_item).paint(format!(" {:^m$} ",todo.brief.clone(), m=2)), 
                    Style::new().fg(Fixed(styles.past_deadline_item.bg.unwrap() as u8)).paint("◤") 
                ],
                _ =>  vec![
                    Style::new()
                        .on(Fixed(styles.flagpole.fg.unwrap() as u8))
                        .fg(Fixed(styles.normal_item.bg.unwrap() as u8))
                        .paint("◢"),
                    norm.paint(format!(" {:^m$} ",todo.brief.clone(), m=2)),
                    Style::new().fg(Fixed(styles.normal_item.bg.unwrap() as u8)).paint("◤") 
                ]
            }
            (_,_) => vec![
                Style::new()
                    .on(Fixed(styles.flagpole.fg.unwrap() as u8))
                    .fg(Fixed(styles.normal_item.bg.unwrap() as u8))
                    .paint("◢"),
                norm.paint(format!(" {:^m$} ",todo.brief.clone(), m=2)),
                Style::new().fg(Fixed(styles.normal_item.bg.unwrap() as u8)).paint("◤") 
            ]
        };
        let mut flagpole = "▃▃▃▃▃▃▃▃".to_string();
        let itemlen = (&todo.brief).chars().count();
        for _ in 1..(itemlen + todo.urgent as usize + desc_flag.chars().count()/3) { flagpole += "▃" };
        println!(" {}",style(styles.flagpole).paint(flagpole)); 
        println!(" {}{}{}{}", 
                 style(styles.number).paint(format!(" {} ",i.to_string())), 
                 style(styles.urgent_flag).paint(&urg_flag), 
                 ANSIStrings(&todo_brief[..]), 
                 match todo.url { 
                     Some(_) => style(styles.description_flag).underline().paint(&desc_flag), 
                     None => style(styles.description_flag).paint(&desc_flag) 
                 }
                );
    }
}
fn print_item(todo: &ToDo, styles: &TDStyles) {
    println!("  {}", style(styles.printitem_brief).paint(&todo.brief));
    println!("  {}", todo.brief.chars().map(|_| "-").collect::<String>());
    todo.description.as_ref().map(|x| println!("{}", textwrap::indent(&x, "  ")));
    todo.url.as_ref().map(|x| println!("\n{}", textwrap::indent(&x, "  ")));
    if todo.postponed.is_some() || todo.deadline.is_some() {
        println!("\n  {}{}", 
                 todo.deadline.as_ref().map(|x| format!("deadline: {}, ",x)).unwrap_or(String::from("no deadline, ")),
                 todo.postponed.as_ref().map(|x| format!("postponed until {}",x)).unwrap_or(String::from("")))
    }
    todo.directory.as_ref().map(|x| println!("  {}",x));
}
fn string_to_option(x: &glib::GString, check_date : bool) -> Result<Option<String>,chrono::ParseError> {
    let y = x.to_string();
    if y == "" { 
        return Ok(None) 
    } else { 
        if check_date {
            let _ = chrono::NaiveDate::parse_from_str(&y, "%Y-%m-%d")?;
        }
        return Ok(Some(y)) 
    }
}
fn filter_todos(xs: &Vec<ToDo>, crit: Box<dyn Fn(&ToDo) -> bool>) -> Vec<(usize,&ToDo)> {
    let mut filtered : Vec<(usize,&ToDo)> = vec![];
    let mut i = 0;
    for x in xs {
        if crit(x) { filtered.push((i,&x)); };
        i = i + 1;
    }
    filtered
}

fn is_not_postponed(now: NaiveDate) -> Box<dyn Fn(&ToDo) -> bool> {
    Box::new(move |td: &ToDo| {
        match &td.postponed {
            Some(pp) => {
                let naive = chrono::NaiveDate::parse_from_str(&pp, "%Y-%m-%d").expect(&format!("Error parsing date: {:?}",pp));
                &now >= &naive
            },
            None => true
        }
    }
    )
}

fn days_to_deadline(now: NaiveDate, td: &ToDo) -> Option<i64> {
    td.deadline.as_ref().map(|dl_str| {
        let naive = chrono::NaiveDate::parse_from_str(&dl_str, "%Y-%m-%d").expect(&format!("Error parsing deadline: {:?}",dl_str));
        (naive - now).num_days()
    })
}


fn gui(gdt: &Rc<GUIData>, n: usize) {
    let application = gtk::Application::new(
        Some("com.example.todo"),
        Default::default(),
        ).expect("failed to initialize GTK application");
    let gdt1 = Rc::clone(gdt);
    application.connect_activate(move |app| {
        let itm = &(*gdt1).todoitem;
        let window = gtk::ApplicationWindow::new(app);
        window.set_title(&itm.brief);
        let grid = gtk::Grid::new();
        let brief_entry = gtk::Entry::new();
        brief_entry.set_text(&itm.brief);
        grid.attach(&gtk::Label::new(Some("summary")),0,0,1,1);
        grid.attach(&brief_entry,1,0,1,1);
        let description_buf = gtk::TextBuffer::new(Some(&gtk::TextTagTable::new()));
        description_buf.set_text(&itm.description.clone().unwrap_or("".to_string()));
        let description_view = gtk::TextView::with_buffer(&description_buf);
        description_view.set_size_request(400,100);
        grid.attach(&gtk::Label::new(Some("description:")),0,1,1,1);
        grid.attach(&description_view,1,1,1,1);
        let url_entry = gtk::Entry::new();
        url_entry.set_text(&itm.url.clone().unwrap_or("".to_string()));
        grid.attach(&gtk::Label::new(Some("URL")),0,2,1,1);
        grid.attach(&url_entry,1,2,1,1);
        let dir_entry = gtk::Entry::new();
        dir_entry.set_text(&itm.directory.clone().unwrap_or("".to_string()));
        grid.attach(&gtk::Label::new(Some("dir")),0,3,1,1);
        grid.attach(&dir_entry,1,3,1,1);
        let ppn_entry = gtk::Entry::new();
        ppn_entry.set_text(&itm.postponed.clone().unwrap_or("".to_string()));
        grid.attach(&gtk::Label::new(Some("postponed")),0,4,1,1);
        grid.attach(&ppn_entry,1,4,1,1);
        let ddl_entry = gtk::Entry::new();
        ddl_entry.set_text(&itm.deadline.clone().unwrap_or("".to_string()));
        grid.attach(&gtk::Label::new(Some("deadline")),0,5,1,1);
        grid.attach(&ddl_entry,1,5,1,1);
        let urg_hbox = gtk::Box::new(gtk::Orientation::Horizontal, 3);
        urg_hbox.add(&gtk::Label::new(Some("urgency")));
        let urg_entry = gtk::Entry::new();
        urg_entry.set_width_chars(1);
        urg_entry.set_text(&format!("{}",itm.urgent));
        urg_hbox.add(&urg_entry);
        grid.attach(&urg_hbox, 0, 6, 1, 1);
        let enter = gtk::Button::new();
        let gdt2 = Rc::clone(&gdt1);
        enter.connect_clicked(clone!(@weak app => move |_| {
            if let (b,Ok(des),Ok(url),Ok(dir),Ok(dl),Ok(pp),Ok(urg)) = 
                ( 
                    brief_entry.get_text().to_string(),
                    description_buf.get_text(
                        &description_buf.get_start_iter(), 
                        &description_buf.get_end_iter(), 
                        true
                        ).and_then(
                            |ref x| string_to_option(x,false).transpose()
                            ).transpose(),
                    string_to_option(&url_entry.get_text(),false),
                    string_to_option(&dir_entry.get_text(),false),
                    string_to_option(&ddl_entry.get_text(),true), 
                    string_to_option(&ppn_entry.get_text(),true),
                    urg_entry.get_text().to_string().parse::<u64>()
                ) {
                    let mut l = get_list(std::path::Path::new(&gdt2.filepath)).unwrap();
                    l[n] = ToDo {brief: b, description: des, url: url, directory: dir, deadline: dl, postponed: pp, urgent: urg};
                    let dhall = mk_dhall(&l);
                    std::fs::write(&gdt2.filepath, &dhall).expect(&format!("Unable to write to: {}",gdt2.filepath));
                    app.quit();
                } else {
                    println!("Error parsing");
                };
        }));
        enter.set_label("enter");
        grid.attach(&enter,1,6,1,1);
        window.add(&grid);

        window.show_all();
    });
    application.run(&[]);
}
fn ask_for_number() -> usize{
    io::stdout().flush().expect("Failed to flush stdout");
    let mut n_str = String::new();
    std::io::stdin().read_line(&mut n_str).expect("read line error");
    n_str.trim().parse::<usize>().expect("you should have entered a number")
}
fn confirmed() -> bool {
    io::stdout().flush().expect("Failed to flush stdout");
    let mut yes_str = String::new();
    std::io::stdin().read_line(&mut yes_str).expect("should enter y or n");
    &yes_str[ .. 1] == "y"
}
fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let matches = App::new("ToDo lists manager")
        .version("1.0")
        .about("......")
        .arg(Arg::with_name("add")
             .short("a")
             .help("add new item")
             .takes_value(true))
        .arg(Arg::with_name("delete")
             .short("d")
             .help("delete")
             .multiple(true))
        .arg(Arg::with_name("n")
             .short("n")
             .help("item No")
             .takes_value(true))
        .arg(Arg::with_name("all")
             .long("all")
             .help("show all (including postponed)")
             .multiple(true))
        .arg(Arg::with_name("full")
             .short("f")
             .help("full description")
             .multiple(true))
        .arg(Arg::with_name("edit")
             .short("e")
             .help("edit item")
             .multiple(true))
        .arg(Arg::with_name("list_name")
             .multiple(true))
        .get_matches();

    let local_date = Local::now().date().naive_local();    
    let addnew: Option<String> = matches.value_of("add").map(|x| x.to_string());
    let del: bool = matches.occurrences_of("delete") > 0;
    let num: Option<usize> = matches.value_of("n").and_then(|n| { Some(n.parse::<usize>()) }).transpose()?;
    let full: bool = matches.occurrences_of("full") > 0;
    let edit: bool = matches.occurrences_of("edit") > 0;
    let showall : bool = matches.occurrences_of("all") > 0;
    let conf: Conf = serde_dhall::from_file(std::path::Path::new(&home_dir().unwrap()).join(".config/td/config.dhall")).parse()?;
    let lists: Vec<ToDoList> = serde_dhall::from_file(std::path::Path::new(&home_dir().unwrap()).join(".config/td/lists.dhall")).parse()?;
    let list_names = matches.values_of("list_name");
    let list_name : Option<String> = list_names.and_then(|mut x| {x.next().map(|y| {String::from(y)})});
    let styles = match &list_name {
        Some(ln) => { get_list_style(&lists,&ln,&conf)? },
        None => conf.styles
    };
    
    let list_file: Option<&std::path::Path> = list_name.clone().map(|ref lname| {get_list_path(&lists,lname)}).transpose()?;
    let list_file_string: Option<String> = list_name.map(|ref lname| {get_list_path_string(&lists,lname)}).transpose()?;

    let mut do_print_new_list = true;
    match list_file {
        None => {
            let alllists = lists.iter().map(|x| {String::from(&x.name)}).reduce(|a, x| { format!("{} {}", a, x) });
            println!("{}", alllists.unwrap_or(String::from(""))); 
            Ok(())
        }
        Some(lst) => get_list(lst).map(|mut todos| {
            let filtered = filter_todos(&todos, if showall { Box::new(|_| true) } else { is_not_postponed(local_date.clone()) });
            if let Some(a) = addnew {
                todos.push(ToDo { brief: a, ..ToDo::default() });
                std::fs::write(&lst, &mk_dhall(&todos)).expect(&format!("Unable to write to: {:?}",lst));
                do_print_new_list = false;
                return
            }
            if full { 
                match num {
                    Some(n) => {
                        todos.iter().nth(n - 1).map(|x| { 
                            print_item(&x, &styles);
                        });
                    },
                    None => ()
                };
                do_print_new_list = false;
                return
            }
            if del {
                print_list(&filtered,local_date,&styles);
                println!(" {}", style(styles.flagpole).paint("▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃"));
                println!(" {}{}", style(styles.number).paint(" ↑ "), style(styles.question).paint("which one to delete? "));
                print!("⇨ ");
                let (real_n, _) = filtered[ask_for_number() - 1];
                print_item(&todos[real_n], &styles);
                println!(" {}", style_b2f(styles.confirmation).paint("▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃"));
                println!(" {}", style(styles.confirmation).paint(" sure want to delete? "));
                print!("⇨ ");
                if confirmed() { 
                    todos.remove(real_n); 
                } else { 
                    println!(" {}", style_b2f(styles.notification).paint("▃▃▃▃▃▃▃▃▃▃▃▃▃▃▃"));
                    println!(" {}", style(styles.notification).paint(" leaving as is ")); 
                }
                let dhall = serde_dhall::serialize::<Vec<ToDo>>(&todos).static_type_annotation().to_string().unwrap();
                std::fs::write(&lst, &dhall).expect(&format!("Unable to write to: {:?}",lst));
                return
            } 
            if edit {
                print_list(&filtered,local_date,&styles);
                println!(" {}", style(styles.flagpole).paint("▃▃▃▃▃▃▃▃▃▃▃▃▃▃"));
                println!(" {}{}", style(styles.number).paint(" ↑ "), style(styles.question).paint("which one? "));
                print!("⇨ ");
                let n = ask_for_number();
                let (real_n, _) = filtered[n - 1];
                gui(
                    &Rc::new(GUIData { filepath : list_file_string.unwrap(), todoitem : todos[real_n].clone() }),
                    real_n
                    );
                return
            }
        }),
    }?;
    if do_print_new_list {
        if let Some(lst) = list_file {
            get_list(lst).map(|todos| {
                let filtered = filter_todos(&todos, if showall { Box::new(|_| true) } else { is_not_postponed(local_date.clone()) });
                print_list(&filtered,local_date,&styles);
                println!("");
            })?;
        } else { () };
    } else { () }
    Ok(())
}
